<?php
 /**
 * Підключення до бази даних
 * Реєстрація користувача листом
 */
 //Ключ захисту
 if(!defined('BEZ_KEY'))
 {
     header("HTTP/1.1 404 Not Found");
     exit(file_get_contents('./../404.html'));
 }

//Підключення до бази даних mySQL за допомогою PDO
try {
    $db = new PDO('mysql:host=localhost;dbname='.BEZ_DATABASE, BEZ_DBUSER, BEZ_DBPASSWORD, array(
    	PDO::ATTR_PERSISTENT => true
    	));
	$db -> exec("SET CHARACTER SET utf8");

} catch (PDOException $e) {
    print "Помилка зєднання: " . $e->getMessage() . "<br/>";
    die();
}

