<?php
/*
     * Головний файл (Переключатель)01
     * Реєстрація користувача листом
*/

	//Запускаєм сесію
	session_start();


    //Встановлюємо кодіровку і вивід всіх помилок
	header('Content-Type: text/html; charset=UTF8');
	error_reporting(E_ALL);


//Включаємо буферизація для вмісту
	ob_start();


//Визначаємо змінну для виключателя
	$mode = isset($_GET['mode'])  ? $_GET['mode'] : false;
	$user = isset($_SESSION['user']) ? $_SESSION['user'] : false;
    $err = array();

//встановлюємо колюч захисту
	define('BEZ_KEY', true);
	 

//Підключаємо конфігураційний файл
	include './config.php';
	 

//Підключаємо скріпти з функціями
	include './func/funct.php';

	//Підключаємо  MySQL
	include './bd/bd.php';

	switch($mode)
	{

        //Підключаємо обработчик з формою реєстрації
		case 'reg':
			include './scripts/reg/reg.php';
			include './scripts/reg/reg_form.html';
		break;
		
		 //Підключаємо обработчик з формою авторизації
		//case 'auth':
		default:
			include './scripts/auth/auth.php';
			//include './scripts/auth/auth_form.html';
			include './scripts/auth/show.php';
		//break;
    
	}
    
	//Получаємо дані з буфера
	$content = ob_get_contents();
	ob_end_clean();

	//Підключаємо шаблон
	if($user == false)
		include './html/tpl/auth.tpl';
	else
		include './html/tpl/datauser.tpl';
/*
	echo '<pre>';
	print_r($_SESSION);
	echo '</pre>';
*/
?>