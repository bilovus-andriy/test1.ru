<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Просто коміт для тесту злиття віток з новими параметрами таблиці-->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Панель Шаблон для початкового завантаження</title>

    <!-- Bootstrap core CSS -->
    <link href="./html/tpl/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./html/tpl/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="./html/tpl/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./html/tpl/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Переключити навігації</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">КП "Тернопільводоканал"</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Загальна інформація</a></li>
            <li><a href="#">Настройки</a></li>
            <li><a href="#"> <span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['name']; ?></a></li>
            <li><a href="?exit=true">Вийти</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Загальний огляд <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Показники</a></li>
            <li><a href="#">Аналітика</a></li>
            <li><a href="#">Дистанційні водоміри</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Повірка</a></li>
            <li><a href="">Оплата</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Показники</a></li>
            <li><a href="">Використання</a></li>
            <li><a href="">Форум</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Панель управління</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
               <img src="./imeg/news.png" alt="logo">
                <h4>Новини</h4>
              <span class="text-muted">Останні новини КП"Тернопільводоканал"</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
                <img src="./imeg/podati_pokaz.png" alt="logo">
              <h4>Подати показник</h4>
              <span class="text-muted">Подача квартирних і юридичних показників</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
                <img src="./imeg/oplatiti.png" alt="logo">
              <h4>Оплатити</h4>
              <span class="text-muted">Оплата послуг водопостачання та водовідведення</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
                <!-- <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail"> -->
                <img src="./imeg/povodomiti.png" alt="logo">
              <h4>Повідомити про</h4>
              <span class="text-muted">Повідомити про:витік аварію інші ситуації..</span>
            </div>
          </div>

          <h2 class="sub-header">Інформація по користувачу</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
              <tr>
                  <th>Особовий</th>
                  <th>Поле 1</th>
                  <th>Поле 2</th>
                  <th>Поле 3</th>
                  <th>Поле 4</th>
                  <th>Поле 5</th>
              </tr>
               <?php
              $sql = 'SELECT *
                            FROM `'. BEZ_DBPREFIX .'kk_vodokanal`
                            WHERE `lichilnika` = :lk';

            $stmt = $db->prepare($sql);
               $stmt->bindValue(':lk', $_SESSION['lk'], PDO::PARAM_INT);
               if($stmt->execute()) {

               $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

               if (count($rows) > 0) {
               $tbl = '';
               foreach ($rows as $val) {
               $tbl .= '<tr>'."\n";
                   foreach ($val as $data) {
                   $tbl .= '<td>' . $data . '</td>' . "\n";
                   }
                   $tbl .= '</tr>' . "\n";
               }
               echo $tbl;
               }else{
               echo 'немає даних';
               }
               }else{
               echo 'ERROR: MySQL Query !!!';
               }
               ?>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="./html/tpl//js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>