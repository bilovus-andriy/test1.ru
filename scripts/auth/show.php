<?php
 /**
  * Скріпт розділення ресурсів
  * Провіряємо права на считування даних,
  * тільки для зареєстрованих користувачів
 */


//Провіряємо чи зайшов користувач
 if($user === false){
 	echo '<h3>Привіт користувач, доступ закритий будь-ласка авторизуйтесь!</h3>'."\n";
 }
 

//Якщо користувач авторизувався
 if($user === true) {
	//Пишемо вітання
	echo '<h4>Ласкаво просимо <span style="color:red;">'. $_SESSION['login'] .'</span> - Ви ввійшли як <span style="color:red;">'.$_SESSION['name'].'</span> <a href="'.BEZ_HOST.'?mode=auth&exit=true">Вийти!И</a></h4>';
	

     //Запит на вибірку контента згідно ролі користувача
	$sql = 'SELECT * FROM `'. BEZ_DBPREFIX.'content`
			WHERE `role` LIKE "%'. $_SESSION['role'] .'%"';
	$stmt = $db->prepare($sql);
	

     //Виводимо контент
	if($stmt->execute()){
		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($rows as $val){
			echo '# - <strong>'. $val['id'] .'</strong><br/>';
			echo $val['content'] .'<br/><br/>';
		}
	}
 }
 ?>
	