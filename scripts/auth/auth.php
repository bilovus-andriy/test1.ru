<?php
 /**
 * Опрацювання форми авторизації
 * Авторизація користувача
 */
 
 //Вихід із авторизації
 if(isset($_GET['exit']) == true){
 	//Вбиваємо сесію
 	session_destroy();

 	//Робимо редірект
 	header('Location:'. BEZ_HOST);
 	exit;
 }


//Якщо нажата кнопка то опрацьовуємо дані
 if(isset($_POST['submit']))
 {
	//Провіряємо на пустоту
	if(empty($_POST['lk']))
		$err[] = 'не ввели рахунок';
	
	if(empty($_POST['pass']))
		$err[] = 'Не ввели пароль';
	
	//Провіряємо email
	/*if(emailValid($_POST['email']) === false)
		$err[] = 'Не коректний E-mail';*/

     //Провіряємо наявність помилок і виводимо користувачу
	if(count($err) > 0)
		echo showErrorMessage($err);
	else
	{
		/*Створюємо запит на вибірку із бази даних
		для провірки істиності користувача*/
		$sql = 'SELECT * FROM `'. BEZ_DBPREFIX .'kk_vodokanal` AS `u`
				LEFT JOIN `'. BEZ_DBPREFIX .'role` AS `r` ON `u`.`role` = `r`.`id_role`
				WHERE `lichilnika` = :lk';
	        //Підготовлюємо PDO значення для SQL запита
		$stmt = $db->prepare($sql);
		$stmt->bindValue(':lk', $_POST['lk'], PDO::PARAM_STR);
		$stmt->execute();

        //получаємо дані SQL запита
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		

        //Якщо логін співпадає, провіряєм пароль
		if(count($rows) > 0)
		{

            //Получаємо дані із таблиць
			if($_POST['pass'] == $rows[0]['adressa'])
			{	

                //Змінні для роботи з користувачами які вже залогінились
				$_SESSION['user'] 	= true;
				$_SESSION['lk'] 	= $_POST['lk'];
				$_SESSION['role'] 	= $rows[0]['role'];
				$_SESSION['name'] 	= $rows[0]['pip'];
				

                //Скидаємо параметри
				header('Location:'. BEZ_HOST );
				exit;
			}
			else
				echo showErrorMessage('Неправильний пароль');
		}else{
			echo showErrorMessage('Логін <b>'. $_POST['email'] .'</b> немає логіна!');
		}
	}
 }
 
?>