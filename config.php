<?php
 /**
 * Конфігураційний файл
 * Site: www.vodokanal.te.ua
 * Реєстрація користувача листом
 */

 //Ключ захисту
 if(!defined('BEZ_KEY'))
 {
     header("HTTP/1.1 404 Not Found");
     exit(file_get_contents('./404.html'));
 }

 //Адрес бази даних
 define('BEZ_DBSERVER','localhost');

 //Логін бази даних
 define('BEZ_DBUSER','root');

 //Пароль бази даних
 define('BEZ_DBPASSWORD','');

 //БД
 define('BEZ_DATABASE','reg');

 //Префікс БД
 define('BEZ_DBPREFIX','bez_');

 //Errors
 define('BEZ_ERROR_CONNECT','Неможу зєднатись з базою даних');

 //Errors
 define('BEZ_NO_DB_SELECT','Дана база даних відсутня на сервері');

 //Адрес хоста сайта
 define('BEZ_HOST','http://'. $_SERVER['HTTP_HOST'] .'/');

//Адрес пошти від кого відправляємо
define('BEZ_MAIL_AUTOR','Реєстрація на http://vodokanal.te.ua <no-reply@vodokanal.te.ua>');
?>